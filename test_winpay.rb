require 'net/http'
require 'openssl'
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

class Test
    attr_accessor :url

    def initialize(url)
        @url = url
    end

    def callUrl
        url = URI(@url) 
        res = Net::HTTP.start(url.host, url.port, :read_timeout => 10, :use_ssl => url.scheme == 'https') do |http|
            request = Net::HTTP::Get.new url
            response = http.request request
        end
        @resultRequest = res.body
    end

    def setArray(array)
        @array = array
    end

    def setTwoValue(valueOne, valueTwo)
        @valueOne = valueOne
        @valueTwo = valueTwo
    end

    def changeText
        @array.each do |el_key, el_value|
            (@valueOne === @valueTwo) ? break :
            @array.delete(el_key)
            @array = @array.merge({el_key.gsub(@valueOne, @valueTwo) => el_value.gsub(@valueOne, @valueTwo)})
        end
    end

    def showResult
        puts @resultRequest, @array
    end
end

test = Test.new("https://dev.by")
test.callUrl()
test.setArray({"test" => "winpay", "" => "eeeee"})
test.setTwoValue("e", "ee")
test.changeText
test.showResult