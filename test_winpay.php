<?php

class Test {
    function __construct ($url) {
        $this->url = $url;
    }

    function callUrl () {
        $scc = stream_context_create(
            array(
                'http'=>array(
                    'method'=> "GET",
                    'timeout' => 600,
                ),
                'ssl'=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ), 
        ));
        $this->resultRequest = file_get_contents($this->url, false, $scc);
    }

    function getUrl () {
        return $this->url;
    }
    
    function setUrl ($url) {
        $this->url = $url;
    }
    
    function setArray ($array) {
        $this->array = $array;
    }

    function setTwoValue ($valueOne, $valueTwo) {
        $this->valueOne = $valueOne;
        $this->valueTwo = $valueTwo;
    }

    function changeText () {
        foreach ($this->array as $key => $value) {
            if ($this->valueOne === $this->valueTwo) break;
            unset($this->array[$key]);
            $key = preg_replace_callback(
                '/'.$this->valueOne.'/',
                function ($matches) {
                    return $this->valueTwo;
                },
                $key
            );
            $value = preg_replace_callback(
                '/'.$this->valueOne.'/',
                function ($matches) {
                    return $this->valueTwo;
                },
                $value
            );
            $this->array[$key] = $value;
            //$this->array[str_replace($this->valueOne, $this->valueTwo, $key)] = str_replace($this->valueOne, $this->valueTwo, $value);
        }
    } 

    function showResult () {
        var_dump($this->resultRequest);
        var_dump($this->array);
    }
}

$test = new Test('https://dev.by');
$test->callUrl();
$test->setArray(["test" => "winpay", "" => "eeeee"]);
$test->setTwoValue("e", "a");
$test->changeText();
$test->showResult();